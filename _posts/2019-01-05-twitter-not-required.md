---
date:   2019-01-05 15:41:59 -0700
categories:
  - Status
  - Twitter
---
Twitter accounts are no longer required to use Moa. You do still need a Mastodon account however so crossposting between just Twitter and Instagram won't work.