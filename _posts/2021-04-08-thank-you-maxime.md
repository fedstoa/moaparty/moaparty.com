---
date:  2021-04-08 21:19:59 -0700
title: Thank You Maxime!
categories:
    - Blog
---
We use the [[Digital Garden Jekyll Template]] to run this site. Thank you to [@maximevaillancourt for creating this theme for Jekyll](https://github.com/maximevaillancourt/digital-garden-jekyll-template).

As part of our desire to [[Support our Dependencies]], we [agreed this week](https://gitlab.com/fedstoa/moa/-/issues/15) to donate $50 to Maxime as a thank you.

![Maxime Ko-Fi Thank You Message](/assets/maxime-ko-fi.png)

You can visit [Maxime's Ko-Fi page](https://ko-fi.com/maximevaillancourt) or [sponsor on Github](https://github.com/sponsors/maximevaillancourt).

The [expense](https://opencollective.com/moa/expenses/38138) is filed on our [[Open Collective]].



