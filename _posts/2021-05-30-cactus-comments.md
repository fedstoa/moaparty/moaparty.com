---
date:   2021-05-30 15:41:59 -0700
categories:
  - Status
  - Matrix
---
We've enabled [[Matrix]]-powered [[Cactus Comments]] on the site here. This means that you can leave a comment on any page, either anonymously, or using your Matrix login. More details on the [[Contact]] page.
