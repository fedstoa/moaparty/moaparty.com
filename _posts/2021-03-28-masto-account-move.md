---
date:  2021-03-28 22:19:59 -0700
categories:
    - Status
---
We redirected the [@moa_party@pdx.social](https://pdx.social/@moa_party) account to our more global [@moaparty@fosstodon.org](https://fosstodon.org/@moaparty) account. Yes, there are a ton of other ways to <a href="/contact" class="internal-link">contact us</a> -- come by and say hi!

Thanks [[foozmeat|@foozmeat]] for finalizing all these bits and pieces of administrative tasks with us.

