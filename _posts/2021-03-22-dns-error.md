---
date:  2021-03-22 09:50:59 -0700
categories:
    - Status
---
One more road bump from switching hosting. There was an error with the DNS configuration that caused the IP address of the domain name to point to the wrong server. This showed a `403` error to users until it was corrected. The configuration was fixed, so this error can't happen again.

<strong>Update March 25th, 2021</strong>:

OK, this happened again, and was really, truly permanently solved at 10:30am PDT. Dynamic DNS was the culprit, turned off in DNS, not just for the root record.

