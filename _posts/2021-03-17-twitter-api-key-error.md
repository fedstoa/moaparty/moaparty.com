---
date:  2021-03-17 17:50:59 -0700
categories:
    - Status
---
As part of the transition to new hosting, we made a mistake with the Twitter API key. People were seeing 500 errors, and toots weren't getting tweeted, and vice versa. Sorry about that, everything should be back to normal now!