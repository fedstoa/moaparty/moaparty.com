---
date:   2018-07-18 15:41:59 -0700
categories:
  - Status
  - Twitter
---
Moa can no longer create replies and mentions on Twitter. Text of the form `@user@twitter.com` on Mastodon used to translate to `@user` on Twitter but now it translates to just `user`. This change was made in order to stay compliant with Twitter's rules.
