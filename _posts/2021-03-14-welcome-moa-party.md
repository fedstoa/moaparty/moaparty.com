---
title: Welcome to the Moa Party!
categories:
    - Blog
---
As of March 10th, the [moa.party](https://moa.party) site has been running on new servers and everything has been transitioned over to the new [[Maintainers]].

Thanks to [[foozmeat|James Moore]] as the original creator, and for his assistance in transitioning the [[Moa]] code, domains, and databases over without a hitch. Thank you James for your work in creating the code, and for running it for many years.

The plan now is to explicitly run the main `moa.party` server as a public utility. It is free of charge to use by anyone. If you actively use the service, there is an expectation that you help support it by contributing. One way to do this is with a direct financial contribution through our [[Open Collective]] page at <https://opencollective.com/moa>.

Through the Open Collective, we will gather basic funds to run the service, as well as find developers and designers to do paid work on the project.

What does this mean? It means that people who wouldn't normally have the privilege to be able to work on open source code without payment, get an opportunity to do paid work **and** help build open source software at the same time. And have some fun with things like [[Design a Logo]], too.

The first thing we're going to look at is how to [[Bring Back Instagram]].[^gitcrosspost] But we want the people using Moa to get involved, tell us what new features you're interested in, and contribute to running the code and the service together.

[^gitcrosspost]: Well, technically, we've already got new code built by [[vera]] that enables [[Cross Posting to Git]], which will be deployed and available on the [[Dev Server]] soon.

The best way to get involved more deeply in the project is to drop into the Matrix channel [#moaparty:matrix.org](https://matrix.to/#/!zPwMsygFdoMjtdrDfo:matrix.org) and introduce yourself. You're also welcome to tweet, toot, or otherwise send messages our way. Use the `#moaparty` hashtag to help us find it.


On behalf of [[bmann]], [[flancian]], and [[vera]], welcome to the [[Moa Party]]! We hope you'll get involved -- let us know what you think!