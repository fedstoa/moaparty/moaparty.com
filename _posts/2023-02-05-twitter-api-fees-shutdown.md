---
date: 2023-02-05 23:19:59 -0700
title: Twitter API Fees and Moa Party Shutdown
categories:
    - Blog
---
As many of you who have been following the situation know, [Twitter announced that free access to the API woud be ending](https://fosstodon.org/@moaparty/109794153125037475) as of February 9th.

**⚠️⚠️⚠️ In all likelihood, we will shutdown the main instance on February 9th, when free API access to Twitter ends. ⚠️⚠️⚠️**

We [ran some numbers](https://fosstodon.org/@moaparty/109797370632031012) on the main instance at [moa.party](https://moa.party):

* There are 8,198 registered accounts, of which ~6K have been active in the last 30 days. 
* 387,281 tweets processed, and 50,582 toots, in the last 30 days.
* Doing some math, that's 380K / 30 days / 24 hours / 60 minutes = 8.96 tweets processed per minute.

We've already seen delays in getting crossposts happening, as we're API rate limited under the current free plan.

No one knows what the _actual_ Twitter API pricing is going to be. Some rumoured prices that have been floating around show that we would likely be in at least $100s per month, if not higher[^higher].

[^higher]: With almost 400K tweets per month, we may be in the highest tier, at $2500USD/month as one rumour suggested.

## Not Shutting Down

What could make us not shutdown? Well, there may be a reasonable tier of API pricing that means we can afford to continue operating.

What can we afford? Well, Moa has an [[Open Collective]] with about $1500CAD[^cad] in the balance. If we could process all of the current traffic for ~6000 users for $100USD / month, that's about 10 months of usage, and we could run a campaign to encourage people to donate.

[^cad]: CAD = Canadian Dollars

Any higher than that, and it's an immediate shutdown, **unless** people signed up to fund it ahead of time. Let's do a thought experiment on that:

(switching to USD as that is the currency Twitter charges in)

Let's assume [[bmann]] and [[flancian]] continue to volunteer for no charge of their time. If the new Twitter API cost is $200/month, that would mean 40 people paying at $5/month.

But if we're now actively providing a service that we have to pay for data, and have an expectation of service to those people paying, we really need to run at better than best effort.

So we'd want something like $500/month part time honorarium for a sysadmin / code maintenance role to back up [[flancian]] (especially since we have users around the globe in many timezones).

And -- volunteer tech support? Maybe a small honorarium for helping out with the issue queue, social channels, FAQ maintenance, better docs, and so on to back up [[bmann]].

So let's say $1000/month total for Twitter API charges, server, and people time.

For that, we'd need at least 200 people subscribed at $5/month.

Which, if you look back up at the server numbers, only 3.3% of the current 6K+ currently active users.

So it's certainly _possible_.

(Nevermind the dilemma of how we feel about paying for the Twitter API…)

## Run your own Moa

Aside from the main instance, Moa is open source software written in Python. Anyone can host their own instance, and pay for their own Twitter API key.

There is some activity in the [GitLab repo](https://gitlab.com/fedstoa/moa) around Dockerizing the current code, to make it simpler for people to run themselves.

As we've said before in various call outs, if you're a Python dev familiar with Docker that can test out some of the PRs, help improve documentation, and various other code modernization, please do get involved.

## Supporting the Service

Thank you to all of you have contributed to date on the [[Open Collective]]:

<script src="https://opencollective.com/moa/banner.js"></script>

The service is run on a volunteer basis by the [[Maintainers]], and we haven't even charged back the domain name or server hosting costs to the current collective balance.

We _don't_ want people donating more at this time, while we don't know the situation. We will use the funds to eg. pay honorariums to Python devs who help improve the codebase.

The best support you can give right now is to help get the word out, contribute to the codebase, trying out the code yourself, and writing documentation for the code. And in general, anyone that has the time and interest to actively join the project and help run it as a one of the [[Maintainers]] would be welcome.

## Next Steps

We have all of our communication channels listed on the [[Contact]] page, including an email address if you want to connect privately. The [Matrix chat](https://matrix.to/#/!zPwMsygFdoMjtdrDfo:matrix.org) or @-mentioning [@moaparty@fosstodon.org](https://fosstodon.org/@moaparty) are two quick methods.

* we'll wait and see what Twitter API prices are on Feb 9th, 2023
* at the very least, there is going to be downtime as we switch API keys / make a paid account
* we may take the service offline on Feb 9th, and not bring it back up
* you can run your own Moa cross-poster with your own paid key