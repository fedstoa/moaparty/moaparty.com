---
title: Cactus Comments
tags:
    - Matrix
    - comments
---

[Cactus Comments](https://cactus.chat/) is a "federated comment system for the web, based on the [[Matrix]] protocol".

You can [try the demo on their site](https://cactus.chat/demo/).

You'll see that there is a comments section enabled on most pages on this site. As our [[Contact|Matrix room is our main community chat]], we think this will be a good fit.

Also their mascot is super cute:

![Cactus Comments Mascot](/assets/cactus500.png)