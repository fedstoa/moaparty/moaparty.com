---
title: Support Pleroma
tags:
    - goals
    - Pleroma
    - OAuth
---

We've had a request to support [[Pleroma]]. Reports are that it doesn't work with Moa currently, which is likely due to [[OAuth]] scope related issues. See the [Pleroma docs](https://docs-develop.pleroma.social/backend/development/authentication_authorization/).

See <https://gitlab.com/fedstoa/moa/-/issues/14>