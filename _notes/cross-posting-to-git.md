---
title: Cross posting to Git
tags:
    - goals
    - features
    - git
---
Enabling cross-posting to a personal Git repo in order to help power a digital garden.

Use [[wikilinks]] double brackets in your Mastodon or Twitter posts, which then get "siphoned" into the Git repo which stores your digital garden notes in [[Markdown]] format.

## Resources
* Feature write up by [[bmann|@bmann]]: [Git siphon for Moa Party](https://bmannconsulting.com/git-siphon-for-moa-party/)
* There is an early version of code that implements this built by [[vera|@vera]]