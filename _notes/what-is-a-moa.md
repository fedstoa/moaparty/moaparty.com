---
title: What is a Moa?
---
A moa is a "now-extinct flightless bird endemic to New Zealand" ([Wikipedia](https://en.wikipedia.org/wiki/Moa))

![](https://upload.wikimedia.org/wikipedia/commons/8/87/Dinornithidae_SIZE_01.png)
* [Wikipedia: A size comparison between four moa species and a human](https://en.wikipedia.org/wiki/Moa#/media/File:Dinornithidae_SIZE_01.png)