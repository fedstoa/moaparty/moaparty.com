---
title: Maintainers
---
Maintainers are those who actively work on code, community, docs, or other aspects of [[Moa Party]].

In alphabetical order, the current maintainers are:
* [[bmann|@bmann]]
* [[flancian|@flancian]]
* [[vera|@vera]]

Code contributors please join the [fedstoa/moa repo on GitLab](https://gitlab.com/fedstoa/moa).

---

The original creator of [[Moa]] is [[foozmeat|James Moore]].