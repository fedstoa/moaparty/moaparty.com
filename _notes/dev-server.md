---
title: Dev Server
---
The development server has in-progress beta code on it, running at <https://dev.moa.party>. It uses different [[Twitter API]] credentials and has a different database, so you need to re-authenticate to use it for testing.