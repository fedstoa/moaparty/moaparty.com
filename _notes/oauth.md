---
title: OAuth
---
OAuth is an authentication standard which is widely used to grant permission to access websites and APIs without having to share a user's password directly.

For the purposes of [[Moa]], OAuth is used by [[Mastodon]] (and Mastodon-compatible systems like [[Pleroma]]), [[Twitter]], and [[Instagram]]. Moa requests access to your accounts on those services on your behalf. OAuth tokens are stored in the Moa database, and you can revoke them at any time.

The OAuth 2.0 standard [IETF RFC6749](https://tools.ietf.org/html/rfc6749) replaced the 1.0 version in October 2012. 

The [oauth.net](https://oauth.net/) site has excellent background info, including the [introduction and history page](https://oauth.net/about/introduction/).

## Resources

* [Twitter OAuth FAQ](https://developer.twitter.com/en/docs/authentication/faq)
* [Mastodon OAuth Docs](https://developer.twitter.com/en/docs/authentication/faq)
* [Instagram API Endpoint - OAuth Access Token](https://developers.facebook.com/docs/instagram-basic-display-api/reference/oauth-access-token/)