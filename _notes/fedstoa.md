---
title: FedStoa
---
FedStoa stands for Federated Stoa or Fediverse Stoa.

The home base for working on code is on GitLab at <https://gitlab.com/fedstoa>, including the [[Moa]] codebase at <https://gitlab.com/fedstoa/moa> and the source for this website at <https://gitlab.com/fedstoa/moaparty/moaparty.com>.