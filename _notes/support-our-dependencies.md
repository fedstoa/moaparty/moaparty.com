---
title: Support Our Dependencies
tags:
    - goals
---
[[Moa Party]] is open source software run as a public utility. We encourage people who find the service useful, want to contribute to building out new features, or otherwise wish to support the party to contribute via [[Open Collective]].

In turn, we want to support our dependencies: what software, tools, or other systems do we make use of, and how can we support them?

* Say thanks to [[foozmeat]], original creator and host for Moa [issue for discussion](https://gitlab.com/fedstoa/moa/-/issues/6)

## Completed

* [[Digital Garden Jekyll Template]] is the template that powers this website. [$50 donation to ko-fi suggested](https://gitlab.com/fedstoa/moa/-/issues/15) -- Done, <a class="internal-link" href="/status/2021/04/08/">April 8th</a>.
