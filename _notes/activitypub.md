---
title: ActivityPub
---

> The ActivityPub protocol is a decentralized social networking protocol based upon the [[ActivityStreams]] 2.0 data format. It provides a client to server API for creating, updating and deleting content, as well as a federated server to server API for delivering notifications and content.

It is a [[W3C]] standard as of January 2018 <https://www.w3.org/TR/activitypub/>, published by the [[W3C Social Web Working Group]].

The [Fediverse SocialHub Discourse forum](https://socialhub.activitypub.rocks/) is where many different ActivityPub-compatible systems come together.

From the forum, [How to become an ActivityPub user](https://socialhub.activitypub.rocks/pub/guide-for-activitypub-users)

From the forum, [Introduction to ActivityPub](https://socialhub.activitypub.rocks/t/introduction-to-activitypub/508)

> ActivityPub supports common social network activities like following, liking, announcing, adding, and blocking. For example, if you have an account on a [[Mastodon]] instance like [mastodon.social](https://mastodon.social), you can follow someone on a [[WriteFreely]] instance like [Qua](https://qua.name) and receive updates whenever they have a new blog post.

Christopher Lemmer Webber, co-author of the ActivityPub standard:

> Increasingly, much of our lives is mediated through social networks, and so network freedom in these spaces – and thus removing central control over them – is critical. One thing you may have noticed in the last decade is that many decentralized free software social networking applications have been written. Sadly, most of those applications can’t actually speak to each other – a fractured federation. I hope that with ActivityPub, we’ve improved that situation.