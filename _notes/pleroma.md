---
title: Pleroma
---

Pleroma is a microblogging server software <https://pleroma.social/>

From the [Git README](https://git.pleroma.social/pleroma/pleroma/):

> Pleroma is a microblogging server software that can federate (= exchange messages with) other servers that support [[ActivityPub]]. What that means is that you can host a server for yourself or your friends and stay in control of your online identity, but still exchange messages with people on larger servers. Pleroma will federate with all servers that implement ActivityPub, like [[Friendica]], [[GNU Social]], [[Hubzilla]], [[Mastodon]], [[Misskey]], [[Peertube]], and [[Pixelfed]].
> Pleroma is written in [[Elixir]] and uses [[PostgresSQL]] for data storage. It's efficient enough to be ran on low-power devices like Raspberry Pi (though we wouldn't recommend storing the database on the internal SD card ;) but can scale well when ran on more powerful hardware (albeit only single-node for now).

A useful blog post: [What is Pleroma?](https://blog.soykaf.com/post/what-is-pleroma/)

> Pleroma is ActivityPub even in its internal data structures. Activities are actually saved as JSON in the database, so the external and internal representations are the same.
>
> Why should you care? Because it makes it easier for Pleroma to add new Activity types. Adding a new Activity type in Pleroma involves no database changes, you just need to add some rules how to handle them. This will help us to add new features like groups or polls in the future.

> This is not really a difference, but Pleroma implements the Client-Server APIs of both GNU Social and Mastodon, so you can use most of their clients with Pleroma, too. For Mastodon clients, this includes Twidere, Tusky, Mastalab, Tootdon and many others.