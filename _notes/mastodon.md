---
title: Mastodon
---
An open source social network that is federated: anyone can run a home server, and each server forms a different community which can have unique rules and features.

## Resources

* Main website: <https://joinmastodon.org>
* Blog: <https://blog.joinmastodon.org>
* Github: <https://github.com/tootsuite/mastodon>
* Documentation <https://docs.joinmastodon.org>
* Choose a community home server to join <https://joinmastodon.org/communities>
* [An increasingly Less-Brief guide to Mastodon](https://github.com/joyeusenoelle/GuideToMastodon)