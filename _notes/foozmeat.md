---
title: foozmeat
---
[James Moore](https://jmoore.me), _@foozmeat_ on many networks including [Mastodon](https://pdx.social/@foozmeat) and [Twitter](https://twitter.com/@foozmeat).

James is the original creator of the [[Moa]] codebase, and ran the `moa.party` server.