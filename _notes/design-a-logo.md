---
title: Design a Logo
tags:
    - goals
---
Commission a designer to make a logo representing [[Moa Party]]. It will almost certainly feature a giant bird and some sort of party theme.