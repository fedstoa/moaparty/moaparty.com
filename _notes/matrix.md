---
title: Matrix
---

The Matrix protocol powers an open network for secure, decentralized communication.

From the [Matrix.org home page](https://matrix.org/):

> Matrix is an open source project that publishes the [Matrix open standard](https://matrix.org/docs/spec) for secure, decentralised, real-time communication, and its Apache licensed [reference implementations](https://github.com/matrix-org).
> 
> Maintained by the non-profit [Matrix.org Foundation](https://matrix.org/foundation/), we aim to create an open platform which is as independent, vibrant and evolving as the Web itself... but for communication.
> 
> As of June 2019, Matrix is [out of beta](https://matrix.org/blog/2019/06/11/introducing-matrix-1-0-and-the-matrix-org-foundation), and the protocol is fully suitable for production usage.

Matrix is great for real-time communications, including voice and video. It features end-to-end encryption (based on the algorithm popularized by Signal), and does a lot of [bridging](https://matrix.org/bridges/).

The bridging aspect might be something to look at for Moa, including using Matrix as the bridge into specific systems.