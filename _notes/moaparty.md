---
title: Moa Party
---
The Moa Party is the collection of people, code, services and community that powers everything, including:

* this website <https://moaparty.com>, used for status updates, documentation, and news
* the <https://moa.party> main server
* the "party" that is all of the [[Maintainers]] and contributors who work on the project

The best way to get involved is to drop into the Matrix channel [#moaparty:matrix.org](https://matrix.to/#/!zPwMsygFdoMjtdrDfo:matrix.org) and introduce yourself.

Check out all of our other [[Contact]] details.

## Public Utility

The [moa.party](https://moa.party) main server is run as a public utility. It is free of charge to use by anyone. If you actively use the service, there is an expectation that you help support it by contributing. One way to do this is with a direct financial contribution through our [[Open Collective]] page.

## Goals

Below are various goals, features, and other objectives we want to accomplish. Anyone can suggest a feature: you can [file an issue](https://gitlab.com/fedstoa/moa/-/issues), or <a href="mailto:reports@moaparty.com">email us any ideas you have</a>.

<ul>
  {% assign notes = site.notes | where_exp: "item", "item.path contains 'notes'" | sort: "last_modified_at" | reverse %}
  {% for entry in notes %}
  {% if entry.tags contains "goals" %}
  <li class="list-entry">
    <div><a class="internal-link" href="{{ entry.url }}">{{ entry.title }}</a> <span
        class="faded">({{ entry.last_modified_at | date: "%Y-%m-%d" }})</span></div>
    <div>{{ entry.excerpt | strip_html | truncatewords: 30 }}</div>

  </li>
  {% endif %}
  {% endfor %}
</ul>