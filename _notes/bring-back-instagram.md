---
title: Bring Back Instagram
tags:
    - goals
    - instagram
---
[[Instagram]] changed their API so that cross-posting was no longer possible.

We need to research whether cross-posting through the API is possible at all, or what it requires from being an official Instagram developer.

---

The [Legacy API was discontinued June 2020](https://www.instagram.com/developer/). Only the [Instagram Graph API](https://developers.facebook.com/docs/instagram-api/) allows for posting media, which is meant for "professionals".