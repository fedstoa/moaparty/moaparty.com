---
title: Goals
---

These are the goals we are currently considering:

<ul>
  {% assign notes = site.notes | where_exp: "item", "item.path contains 'notes'" | sort: "last_modified_at" | reverse %}
  {% for entry in notes %}
  {% if entry.tags contains "goals" %}
  <li class="list-entry">
    <div><a class="internal-link" href="{{ entry.url }}">{{ entry.title }}</a> <span
        class="faded">({{ entry.last_modified_at | date: "%Y-%m-%d" }})</span></div>
    <div>{{ entry.excerpt | strip_html | truncatewords: 30 }}</div>

  </li>
  {% endif %}
  {% endfor %}
</ul>

There are other [issues tagged as features on the Gitlab site](https://gitlab.com/fedstoa/moa/-/issues?label_name%5B%5D=Feature). If you'd like to have it become a goal, please do a short write up on how you would like it to work, so we can publish a page here.

You can [file an issue](https://gitlab.com/fedstoa/moa/-/issues) if you'd like, or <a href="mailto:reports@moaparty.com">email us any ideas you have</a>.