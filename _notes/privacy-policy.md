---
title: Privacy Policy
---
Part of server code, available at [moa.party/privacy](https://moa.party/privacy). This moaparty.com website is a static website, details in the [[Colophon]].

---

## What information do we collect?

We collect your Twitter, Mastodon and Instagram usernames along with [[OAuth]] tokens for each service.

## What do we use your information for?

Your information is used for accessing online accounts. The stored OAuth tokens can be revoked at any time.

## Do we use cookies?

Yes. While you're modifying your settings a cookie is used for your session.

## Do we disclose any information to outside parties?

No.