---
title: Open Collective
---
[Open Collective](https://opencollective.com/) is a platform that enables projects to raise funds and manage them transparently. The Moa Open Collective page is at <https://opencollective.com/moa>

For [[Moa Party]], we ask that active users of the main service contribute some amount if they value the service, want to support goals, or want to say thank you for the service.

This helps cover ongoing costs, as well as raises funds to hire people to do paid work on the open source code base of [[Moa]].

Currently, none of the core [[Maintainers]] need or want paid remuneration for working on the project, and in fact some actively contribute funds to the project themselves.

## Ongoing Costs

* annual domain name registration of `moa.party` and `moaparty.com`
* server hosting for `moa.party`

## Goals

Other than ongoing costs, we hope to raise funds so we can hire people to do paid work for the project. This means making the experience of working on an open source project available to a larger potential pool of people, who wouldn't typically be privileged enough to be able to work for free.

Current goals include:

* Feature: [[Cross posting to Git]]
* Feature: [[Bring Back Instagram]]
* Design: [[Design a Logo]]

Questions? Use our [[Contact]] to get in touch.