---
title: Contact
---

## Community Chat

We use [[Matrix]] for our main Moa Party community & dev chat [#moaparty:matrix.org](https://matrix.to/#/!zPwMsygFdoMjtdrDfo:matrix.org).

The [Moa Party Space](https://matrix.to/#/!xmHCfzrSIZsiCUlBaQ:matrix.org) lists all the chat rooms.

We also run Matrix-powered [[Cactus Comments]] to enable comments on this website. Feel free to do a test post on the bottom of this page 👇
## Social channels:
* Twitter <https://twitter.com/moaparty> 
* Mastodon <https://fosstodon.org/@moaparty>
* Instagram <https://instagram.com/moa.party>

## Open Collective

* Financial Support through Open Collective <https://opencollective.com/moa>

Read more about [[Open Collective]].

## Code:
* GitLab <https://gitlab.com/fedstoa/moa>
* GitHub <https://github.com/fedstoa/moa> (mirrored from GitLab)
* Source for this website <https://gitlab.com/fedstoa/moaparty/moaparty.com>

If you're technically minded, feel free to file an issue or feature request directly.

Otherwise, <a href="mailto:reports@moaparty.com">report a problem by email</a>, or just by leaving a comment directly on this page. Or say something nice, that's good too!

P.S. Yes, there are real humans here! See the [[Maintainers]] page.