---
title: Gitlab Pages
---
Main page is: <https://pages.gitlab.io>

Documentation is at <https://docs.gitlab.com/ce/user/project/pages/>

Uses [`.gitlab-ci.yml` as recommended by Gitlab](https://gitlab.com/pages/jekyll/-/blob/master/.gitlab-ci.yml).

Instructions for mapping a custom domain to GitLab Pages: <https://docs.gitlab.com/ce/user/project/pages/custom_domains_ssl_tls_certification/index.html>