---
title: Moa
---
Moa is the actual name of the server code that runs [[Moa Party]].

Code is developed on GitLab at <https://gitlab.com/fedstoa/moa>, and mirrored on GitHub at <https://github.com/fedstoa/moa>.

Check out the current [[Maintainers]].