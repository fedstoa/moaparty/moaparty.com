---
title: bmann
tags:
    - person
---
Boris Mann is one of the core [[Maintainers]]. His personal digital garden is at <https://bmannconsulting.com>.

* Mastodon [@bmann@social.coop](https://social.coop/@bmann) 
* Twitter [@bmann](https://twitter.com/bmann)
* Github [@bmann](https://github.com/bmann)
* Gitlab [@borismann](https://gitlab.com/borismann)