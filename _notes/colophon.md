---
title: Colophon
---
This site is created using [[Jekyll]] using the [[Digital Garden Jekyll Template]].

It is hosted on [[Gitlab Pages]]. The domain is registered at Google Domains.

You can make edits and contribute to the code in the [fedstoa/moaparty/moaparty.com repo](https://gitlab.com/fedstoa/moaparty/moaparty.com).